// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or any plugin's vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery_ujs
//= require turbolinks
//= require_tree .
//= require zeroclipboard

$('#nav').affix({
      offset: {
        top: $('header').height()-$('#nav').height()
      }
});	

/* highlight the top nav as scrolling occurs */
$('body').scrollspy({ target: '#nav' })

/* smooth scrolling for scroll to top */
$('.scroll-top').click(function(){
  $('body,html').animate({scrollTop:0},1000);
})

/* smooth scrolling for nav sections */
$('#nav .navbar-nav li>a').click(function(){
  var link = $(this).attr('href');
  var posi = $(link).offset().top;
  $('body,html').animate({scrollTop:posi},700);
});


$(document).on('ready page:load', function() {
	var subTotal = 0;
	var objPrice = 0;
	var ceilPrice = 0;
	var counter = 1;

	$(".hcbutton").on("click",function(){
		var $button = $(this);
		var coffeeObject = $button.parent().data();
		objPrice = coffeeObject.price;
		$('#btn-submit').before("<div data-price='" + objPrice + "'><b>" + coffeeObject.temperature.toUpperCase() + '</b> '  +coffeeObject.item + " - " + coffeeObject.size + "<br>RM " + coffeeObject.price +"  <a href='#' class='remove-field'>Remove</a><input type='hidden' name=[order][" + counter + "][item_name] value='" + coffeeObject.item + "'><input type='hidden' name=[order][" + counter + "][size] value=" + coffeeObject.size + "><input type='hidden' name=[order][" + counter + "][price] value=" + coffeeObject.price + "><input type='hidden' name=[order][" + counter + "][temperature] value=" + coffeeObject.temperature + "></div><br>");
		counter += 1;
		calcSubTotal("+");
		refreshSubmit();
	});

	$(document).on('click', '.remove-field', function(){
		objPrice = $(this).parent('div').data().price;
		$(this).parent('div').remove();
		calcSubTotal("-");
		refreshSubmit();
	});

	function calcSubTotal(plusminus) {
		if(plusminus == "+"){
			subTotal = subTotal + objPrice ;
		}
		else{
			subTotal = subTotal - objPrice ;
		}
		ceilPrice = Math.ceil(subTotal);
		$("#estimate-total").text("TOTAL: RM " + parseFloat(subTotal).toFixed(2));
		$("#estimate-runner").text("TOTAL PAY TO RUNNER: RM " + parseFloat(ceilPrice).toFixed(2));
	}
});

// temporarily hard-coded to check amount 0.00
function refreshSubmit() {
	if ($('#neworder').length > 0) {
		var expired = $(".order-info").data("expired");
  	if (expired) {
  		$('#btn-submit').prop('disabled', true);
  	} else {
			var total = $("#estimate-total").html();
   		if (total == "TOTAL: RM 0.00" || total == "TOTAL: RM -0.00") {
   			$('#btn-submit').prop('disabled', true);
   		} else {
   			$('#btn-submit').prop('disabled', false);
   		}
   	}
 	}
}