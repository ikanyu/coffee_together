class OrderItemDetail < ActiveRecord::Base
	belongs_to :order
	belongs_to :item_detail
	validates :order_id, :item_detail_id, presence: true
	validates_associated :order, :item_detail
end