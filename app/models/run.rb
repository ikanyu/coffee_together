class Run < ActiveRecord::Base
	has_many :orders, :dependent => :destroy
	has_many :item_details, through: :orders, join_table: :items
	has_many :items, through: :orders, through: :item_details, join_table: :item_details
	belongs_to :user
	belongs_to :store
	validates :store_id, :user_id, presence: true
	validates_associated :user, :store

  def generate_url
    self.url = SecureRandom.hex(5)
  end
  
end
