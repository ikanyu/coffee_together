class Store < ActiveRecord::Base
	has_many :users, through: :runs
	has_many :runs
	has_many :items
	validates :name, presence: true
end
