class ItemDetail < ActiveRecord::Base
	belongs_to :item
	has_many :orders, through: :order_item_details
	has_many :order_item_details
	validates :item_id, presence: true
	validates_associated :item
end
