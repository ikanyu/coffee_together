class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable
  has_many :stores, through: :runs
	has_many :runs
	has_many :orders
	validates :encrypted_password, presence: true
	validates :email, presence: true, uniqueness: true
	validates :name, presence: true
end
