class Order < ActiveRecord::Base
	belongs_to :user
	belongs_to :run	
	has_many :item_details, through: :order_item_details
	has_many :order_item_details, :dependent => :destroy
	validates :run_id, :user_id, presence: true
	validates_associated :user, :run

end
