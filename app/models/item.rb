class Item < ActiveRecord::Base
	belongs_to :store
	has_many :item_details
	validates :store_id, :item_name, presence: true
	validates_associated :store
end
