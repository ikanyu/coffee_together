class ContactsController < ApplicationController
  def new
    @contact = Contact.new
  end

  def create
    @contact = Contact.new(params[:contact])
    @contact.request = request
    if @contact.deliver
      flash.now[:notice] = 'Thank you for your feedback!'
    else
      flash.now[:error] = 'Cannot send feedback. Please try again. Your feedback is so much valuable to us.'
      render :new
    end
  end
end