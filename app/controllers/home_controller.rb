class HomeController < ApplicationController
	def index

	    if !params[:url].nil?
	  		@run = Run.find_by_url(params[:url])
	  		redirect_to new_run_order_path(@run.id)
	  	end

	end
end
	  
