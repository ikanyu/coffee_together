class OrdersController < ApplicationController
	before_action :authenticate_user!
	
	def index

	end

	def new
		@run = Run.find(params[:run_id])
		@store = @run.store
		@order = @run.orders.new
		@items = @store.items.includes(:item_details)
		@hash = Hash.new
		@items.each do |i|
			@itemsize =  ItemDetail.where(item_id:i.id).select('item_id','size','price').group("item_id","size","price").order("price")
			@hash[i.item_name] = Hash.new
			@itemsize.each do |s|
				@hash[i.item_name][s.size] = Hash.new
				@temp = ItemDetail.where(item_id:i.id).select(:temperature).distinct
				@temp.each do |t|
					@hash[i.item_name][s.size][t.temperature] = Hash.new
					@hash[i.item_name][s.size][t.temperature] = (s.price).round(2)
				end
			end			
		end

		@expired = false
		if (Time.now - @run.expired_at) >= 0
			@expired = true
		end

	end

	def create
		@run = Run.find(params[:run_id])
		@order = @run.orders.new
		@order.run_id = @run.id
		@order.user = current_user
		@order.save
		item_count = 0
		@order_params = params["order"]
		@order_params.each do |x|		
			@oid = @order.order_item_details.new
			i_id = Item.find_by(item_name: x[1]["item_name"]).id
			item_size = x[1]["size"]
			item_price = x[1]["price"].to_f
			item_temperature = x[1]["temperature"]
			i_d_id = ItemDetail.find_by(item_id: i_id, size: item_size, price: item_price, temperature: item_temperature).id
			@oid.item_detail_id = i_d_id
			@oid.quantity = 1
			@oid.save
			item_count += 1
		end
		sum = 0
		@order.order_item_details.each do |oidl|
			sum = sum + oidl.item_detail.price
		end
		@order.update(total_store: sum.to_f)
		@order.update(total_run: sum.ceil)
		@run.update(remaining: @run.remaining - item_count)
		redirect_to run_order_path(@run.id, @order.id)
	end

	def show
		@order = Order.find(params[:id])
		array = []
    @temp_hash = Hash.new(0)
      @order.item_details.each do |item_detail|
        array << item_detail.item.item_name
        array << item_detail.size
        array << item_detail.temperature
        array << item_detail.price.ceil
      end
      @order.order_item_details.each do |oid|
        @temp_hash[oid.item_detail_id] += oid.quantity.to_i
      end
    @store_array = []
    @temp_hash.each do |k, v|
      @store_array << ItemDetail.find(k).item.item_name
      @store_array << ItemDetail.find(k).size
      @store_array << ItemDetail.find(k).temperature
      @store_array << ItemDetail.find(k).price
      @store_array << v
    end
    @store_array = @store_array.each_slice(5).to_a
	end

	def edit
		
	end

	def update
		
	end

	def destroy
		
	end

end