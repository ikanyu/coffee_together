class RunsController < ApplicationController
  before_action :authenticate_user!

  def index
    if !params[:url].nil?
      @url = Run.find_by(url: params[:url])
      redirect_to new_run_order_path(@url)
    end
    @runs = Run.all
  end

  def new
  	@run = Run.new
  end

  def create
  	@run = Run.new(run_params)
    @run.user = current_user
    @run.generate_url
  	if @run.save
  	  redirect_to @run
  	else
  	  render 'new'
  	end
  end

  def show
  	set_run
    array = []
    @temp_hash = Hash.new(0)
    @run.orders.each do |order|
      order.item_details.each_with_index do |item_detail, index|
        array << item_detail.item.item_name
        array << item_detail.size
        array << item_detail.temperature
        array << order.user.name
        # array << item_detail.price.ceil
        if index == 0 
          array << order.total_run
        else 
          array << ""
        end
      end
      order.order_item_details.each do |oid|
        @temp_hash[oid.item_detail_id] += oid.quantity.to_i
      end
    end

    @store_array = []
    @temp_hash.each do |k, v|
      @store_array << ItemDetail.find(k).item.item_name
      @store_array << ItemDetail.find(k).size
      @store_array << ItemDetail.find(k).temperature
      @store_array << ItemDetail.find(k).price
      @store_array << v
    end

    @items_array = array.each_slice(5).to_a
    @store_array = @store_array.each_slice(5).to_a
    @datetime = @run.expired_at.strftime("%b %-d, %H:%M")
    @delivertime = (@run.expired_at + 1.hours).strftime("%b %-d, %H:%M")
  end

  def edit
  	set_run
  end

  def update
  	set_run
  	if @run.update_attributes(run_params)
  	  redirect_to @run
  	else
  	  render 'edit'
  	end
  end

  def destroy
  	Run.find(params[:id]).destroy
  	redirect_to '/runs/new'
  end

  

  private

  def set_run
  	@run = Run.find(params[:id])
  end

  def run_params
    if params[:run][:day] == "Today"
      params[:run][:expired_at] = Time.local(params[:run]["expired_time(1i)"],params[:run]["expired_time(2i)"],params[:run]["expired_time(3i)"],params[:run]["expired_time(4i)"],params[:run]["expired_time(5i)"])
    else 
      params[:run][:expired_at] = (Time.local(params[:run]["expired_time(1i)"],params[:run]["expired_time(2i)"],params[:run]["expired_time(3i)"],params[:run]["expired_time(4i)"],params[:run]["expired_time(5i)"]) + (24*60*60))
    end
    ## userid = 1
    # params[:run][:user_id] = 1
    params[:run][:user_id] = current_user.id
    params[:run][:limit] = params[:run][:remaining]
  	params.require(:run).permit(:store_id, :location, :expired_at, :limit, :remaining, :user_id)
  end

end
