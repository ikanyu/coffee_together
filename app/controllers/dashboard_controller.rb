class DashboardController < ApplicationController
	before_action :authenticate_user!

  def index
  	@runs = Run.where(user_id: current_user).limit(3).order("expired_at DESC")
  	@orders = Order.where(user_id: current_user).limit(3).order("created_at DESC")
  end
  
end
