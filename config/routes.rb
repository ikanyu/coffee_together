Rails.application.routes.draw do
  get 'dashboard/index'

  devise_for :users
  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  get "/?url=#{:url}" => 'home#index', as: :order_link


  root 'home#index'
  resources :runs do
    resources :orders
  end

  match '/contacts', to: 'contacts#new', via: 'get'
  resources "contacts", only: [:new, :create]

  # get '/url/:id' => 'orders#redirect_url', as: :redirect_order

  # get '/users/:user_id/orders' => 'orders#index', as: :user_orders
  # post '/users/:user_id/orders' => 'orders#create', as: :user_orders_create
  # get '/users/:user_id/runs/:run_id/orders/new' => 'orders#new', as: :new_user_order
  # get '/users/:user_id/orders/:id' => 'orders#show', as: :user_order
  # get '/users/:user_id/orders/:id/edit' => 'orders#edit', as: :edit_user_order
  # patch '/users/:user_id/orders/:id/update' => 'orders#update'
  # delete '/users/:user_id/orders/:id/destroy' => 'orders#destroy'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
