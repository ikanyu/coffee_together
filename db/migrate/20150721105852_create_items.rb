class CreateItems < ActiveRecord::Migration
  def change
    create_table :items do |t|
    	t.integer :store_id
    	t.string :item_name
    	t.string :size
    	t.decimal :price
    	t.string :temperature
      t.timestamps null: false
    end
  end
end
