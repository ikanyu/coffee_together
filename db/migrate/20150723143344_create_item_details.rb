class CreateItemDetails < ActiveRecord::Migration
  def change
    create_table :item_details do |t|
      t.integer :item_id
      t.string :size
      t.decimal :price
      t.string :temperature
      t.timestamps null: false
    end
  end
end
