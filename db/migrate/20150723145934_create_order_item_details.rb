class CreateOrderItemDetails < ActiveRecord::Migration
  def change
    create_table :order_item_details do |t|
      t.integer :order_id
      t.integer :item_detail_id
      t.integer :quantity
      t.timestamps null: false
    end
  end
end
