class CreateOrders < ActiveRecord::Migration
  def change
    create_table :orders do |t|
    	t.integer :run_id, :user_id
    	t.decimal :total_store, :total_run
      t.timestamps null: false
    end
  end
end
