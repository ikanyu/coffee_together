class AddUrlToRuns < ActiveRecord::Migration
  def change
    add_column :runs, :url, :string
  end
end
