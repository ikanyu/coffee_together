class RemoveItems < ActiveRecord::Migration
  def change
  	remove_column :items, :price, :decimal
  	remove_column :items, :temperature, :string
  	remove_column :items, :size, :string
  end
end
