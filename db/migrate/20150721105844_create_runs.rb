class CreateRuns < ActiveRecord::Migration
  def change
    create_table :runs do |t|
      t.integer :store_id, :user_id, :limit, :remaining
      t.timestamp :expired_at
      t.timestamps null: false
    end
  end
end
