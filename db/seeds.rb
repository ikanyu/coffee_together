# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
Store.create(name: "Starbucks", location: "CyberJaya")
# Store.create(name: "Coffee Bean", location: "CyberJaya")
# Store.create(name: "Happy Joes", location: "Dengkil")

User.create(name: "Administrator", email: "coffee.bersama@gmail.com", password: "junerails")
# User.create(name: "Abdullah", email: "abdullah@gmail.com", password: "junerails")
# User.create(name: "Jay", email: "jay@gmail.com", password: "password")
# User.create(name: "WY", email: "wenyu@gmail.com", password: "password")
# User.create(name: "JC", email: "jc@gmail.com", password: "password")
# User.create(name: "Henry", email: "henry@gmail.com", password: "password")
# User.create(name: "KH", email: "kh@example.com", password: "password")

Item.create(store_id: 1, item_name: "Asian Dolce Latte")
Item.create(store_id: 1, item_name: "Caramel Macchiato")
Item.create(store_id: 1, item_name: "Cocoa Cappuccino")
Item.create(store_id: 1, item_name: "Caffe Mocha")
Item.create(store_id: 1, item_name: "Caffe Latte")
Item.create(store_id: 1, item_name: "Cappuccino")
# Item.create(store_id: 1, item_name: "Affogato")
# Item.create(store_id: 1, item_name: "Doubleshot Iced Shaken Expresso")
Item.create(store_id: 1, item_name: "Caffe Americano")
Item.create(store_id: 1, item_name: "Freshly Brewed Coffee")
# Item.create(store_id: 1, item_name: "Coffee By The Press")

Item.create(store_id: 1, item_name: "Dark Mocha Frappuccino")
Item.create(store_id: 1, item_name: "Java Chip Frappuccino")
# Item.create(store_id: 1, item_name: "Mocha Praline Frappuccino")
Item.create(store_id: 1, item_name: "Chocolate Cream Chip Frappuccino")
Item.create(store_id: 1, item_name: "Green Tea Cream Frappuccino")
Item.create(store_id: 1, item_name: "Mocha Frappuccino")
Item.create(store_id: 1, item_name: "Caramel Frappuccino")
Item.create(store_id: 1, item_name: "Espresso Frappuccino")

Item.create(store_id: 1, item_name: "Green Tea Latte")
Item.create(store_id: 1, item_name: "Black Tea Latte")
Item.create(store_id: 1, item_name: "Iced Shaken Lemon Tea")
Item.create(store_id: 1, item_name: "Freshly Brewed Tea")
Item.create(store_id: 1, item_name: "Signature Hot Chocolate")
Item.create(store_id: 1, item_name: "Hazelnut Hot Chocolate")
Item.create(store_id: 1, item_name: "Caramel Hot Chocolate")

# Item.create(store_id: 1, item_name: "Cappuccino")
# Item.create(store_id: 1, item_name: "Caffe Latte")
# Item.create(store_id: 1, item_name: "Frappuccino")
# Item.create(store_id: 1, item_name: "Hazelnut Hot Chocolate")

#Asian Dolce Latte
ItemDetail.create(item_id: 1, size: "Tall", price: 13.80, temperature: "Hot")
ItemDetail.create(item_id: 1, size: "Tall", price: 13.80, temperature: "Cold")
ItemDetail.create(item_id: 1, size: "Grande", price: 14.85, temperature: "Hot")
ItemDetail.create(item_id: 1, size: "Grande", price: 14.85, temperature: "Cold")
ItemDetail.create(item_id: 1, size: "Venti", price: 15.90, temperature: "Hot")
ItemDetail.create(item_id: 1, size: "Venti", price: 15.90, temperature: "Cold")

#Caramel Macchiato
ItemDetail.create(item_id: 2, size: "Tall", price: 13.25, temperature: "Hot")
ItemDetail.create(item_id: 2, size: "Tall", price: 13.25, temperature: "Cold")
ItemDetail.create(item_id: 2, size: "Grande", price: 14.30, temperature: "Hot")
ItemDetail.create(item_id: 2, size: "Grande", price: 14.30, temperature: "Cold")
ItemDetail.create(item_id: 2, size: "Venti", price: 15.35, temperature: "Hot")
ItemDetail.create(item_id: 2, size: "Venti", price: 15.35, temperature: "Cold")

#Cocoa Cappuccino
ItemDetail.create(item_id: 3, size: "Tall", price: 12.70, temperature: "Hot")
ItemDetail.create(item_id: 3, size: "Tall", price: 12.70, temperature: "Cold")
ItemDetail.create(item_id: 3, size: "Grande", price: 13.80, temperature: "Hot")
ItemDetail.create(item_id: 3, size: "Grande", price: 13.80, temperature: "Cold")
ItemDetail.create(item_id: 3, size: "Venti", price: 14.85, temperature: "Hot")
ItemDetail.create(item_id: 3, size: "Venti", price: 14.85, temperature: "Cold")

#Caffe Mocha
ItemDetail.create(item_id: 4, size: "Tall", price: 12.70, temperature: "Hot")
ItemDetail.create(item_id: 4, size: "Tall", price: 12.70, temperature: "Cold")
ItemDetail.create(item_id: 4, size: "Grande", price: 13.80, temperature: "Hot")
ItemDetail.create(item_id: 4, size: "Grande", price: 13.80, temperature: "Cold")
ItemDetail.create(item_id: 4, size: "Venti", price: 14.85, temperature: "Hot")
ItemDetail.create(item_id: 4, size: "Venti", price: 14.85, temperature: "Cold")

#Caffe Latte
ItemDetail.create(item_id: 5, size: "Tall", price: 11.15, temperature: "Hot")
ItemDetail.create(item_id: 5, size: "Tall", price: 11.15, temperature: "Cold")
ItemDetail.create(item_id: 5, size: "Grande", price: 12.20, temperature: "Hot")
ItemDetail.create(item_id: 5, size: "Grande", price: 12.20, temperature: "Cold")
ItemDetail.create(item_id: 5, size: "Venti", price: 13.25, temperature: "Hot")
ItemDetail.create(item_id: 5, size: "Venti", price: 13.25, temperature: "Cold")

#Cappuccino
ItemDetail.create(item_id: 6, size: "Tall", price: 11.15, temperature: "Hot")
ItemDetail.create(item_id: 6, size: "Tall", price: 11.15, temperature: "Cold")
ItemDetail.create(item_id: 6, size: "Grande", price: 12.20, temperature: "Hot")
ItemDetail.create(item_id: 6, size: "Grande", price: 12.20, temperature: "Cold")
ItemDetail.create(item_id: 6, size: "Venti", price: 13.25, temperature: "Hot")
ItemDetail.create(item_id: 6, size: "Venti", price: 13.25, temperature: "Cold")

#Caffe Americano
ItemDetail.create(item_id: 7, size: "Tall", price: 7.40, temperature: "Hot")
ItemDetail.create(item_id: 7, size: "Tall", price: 7.40, temperature: "Cold")
ItemDetail.create(item_id: 7, size: "Grande", price: 8.50, temperature: "Hot")
ItemDetail.create(item_id: 7, size: "Grande", price: 8.50, temperature: "Cold")
ItemDetail.create(item_id: 7, size: "Venti", price: 9.55, temperature: "Hot")
ItemDetail.create(item_id: 7, size: "Venti", price: 9.55, temperature: "Cold")

#Freshly Brewed Coffee
ItemDetail.create(item_id: 8, size: "Tall", price: 7.40, temperature: "Hot")
ItemDetail.create(item_id: 8, size: "Tall", price: 7.40, temperature: "Cold")
ItemDetail.create(item_id: 8, size: "Grande", price: 8.50, temperature: "Hot")
ItemDetail.create(item_id: 8, size: "Grande", price: 8.50, temperature: "Cold")
ItemDetail.create(item_id: 8, size: "Venti", price: 9.55, temperature: "Hot")
ItemDetail.create(item_id: 8, size: "Venti", price: 9.55, temperature: "Cold")

#Dark Mocha Frappuccino
ItemDetail.create(item_id: 9, size: "Tall", price: 14.85, temperature: "Cold")
ItemDetail.create(item_id: 9, size: "Grande", price: 15.90, temperature: "Cold")
ItemDetail.create(item_id: 9, size: "Venti", price: 16.95, temperature: "Cold")

#Java Chip Frappuccino
ItemDetail.create(item_id: 10, size: "Tall", price: 14.85, temperature: "Cold")
ItemDetail.create(item_id: 10, size: "Grande", price: 15.90, temperature: "Cold")
ItemDetail.create(item_id: 10, size: "Venti", price: 16.95, temperature: "Cold")

#Chocolate Cream Chip Frappuccino
ItemDetail.create(item_id: 11, size: "Tall", price: 14.85, temperature: "Cold")
ItemDetail.create(item_id: 11, size: "Grande", price: 15.90, temperature: "Cold")
ItemDetail.create(item_id: 11, size: "Venti", price: 16.95, temperature: "Cold")

#Green Tea Cream Frappuccino
ItemDetail.create(item_id: 12, size: "Tall", price: 14.85, temperature: "Cold")
ItemDetail.create(item_id: 12, size: "Grande", price: 15.90, temperature: "Cold")
ItemDetail.create(item_id: 12, size: "Venti", price: 16.95, temperature: "Cold")

#Mocha Frappuccino
ItemDetail.create(item_id: 13, size: "Tall", price: 12.70, temperature: "Cold")
ItemDetail.create(item_id: 13, size: "Grande", price: 13.80, temperature: "Cold")
ItemDetail.create(item_id: 13, size: "Venti", price: 14.85, temperature: "Cold")

#Caramel Frappuccino
ItemDetail.create(item_id: 14, size: "Tall", price: 12.70, temperature: "Cold")
ItemDetail.create(item_id: 14, size: "Grande", price: 13.80, temperature: "Cold")
ItemDetail.create(item_id: 14, size: "Venti", price: 14.85, temperature: "Cold")

#Espresso Frappuccino
ItemDetail.create(item_id: 15, size: "Tall", price: 12.70, temperature: "Cold")
ItemDetail.create(item_id: 15, size: "Grande", price: 13.80, temperature: "Cold")
ItemDetail.create(item_id: 15, size: "Venti", price: 14.85, temperature: "Cold")

# Item.create(store_id: 1, item_name: "Green Tea Latte")
ItemDetail.create(item_id: 16, size: "Tall", price: 13.25, temperature: "Hot")
ItemDetail.create(item_id: 16, size: "Tall", price: 13.25, temperature: "Cold")
ItemDetail.create(item_id: 16, size: "Grande", price: 14.30, temperature: "Hot")
ItemDetail.create(item_id: 16, size: "Grande", price: 14.30, temperature: "Cold")
ItemDetail.create(item_id: 16, size: "Venti", price: 15.35, temperature: "Hot")
ItemDetail.create(item_id: 16, size: "Venti", price: 15.35, temperature: "Cold")

# Item.create(store_id: 1, item_name: "Black Tea Latte")
ItemDetail.create(item_id: 17, size: "Tall", price: 11.15, temperature: "Hot")
ItemDetail.create(item_id: 17, size: "Tall", price: 11.15, temperature: "Cold")
ItemDetail.create(item_id: 17, size: "Grande", price: 12.20, temperature: "Hot")
ItemDetail.create(item_id: 17, size: "Grande", price: 12.20, temperature: "Cold")
ItemDetail.create(item_id: 17, size: "Venti", price: 13.25, temperature: "Hot")
ItemDetail.create(item_id: 17, size: "Venti", price: 13.25, temperature: "Cold")

# Item.create(store_id: 1, item_name: "Iced Shaken Lemon Tea")
# ItemDetail.create(item_id: 18, size: "Tall", price: 11.15, temperature: "Hot")
ItemDetail.create(item_id: 18, size: "Tall", price: 11.15, temperature: "Cold")
# ItemDetail.create(item_id: 18, size: "Grande", price: 12.20, temperature: "Hot")
ItemDetail.create(item_id: 18, size: "Grande", price: 12.20, temperature: "Cold")
# ItemDetail.create(item_id: 18, size: "Venti", price: 13.25, temperature: "Hot")
ItemDetail.create(item_id: 18, size: "Venti", price: 13.25, temperature: "Cold")

# Item.create(store_id: 1, item_name: "Freshly Brewed Tea")
ItemDetail.create(item_id: 19, size: "Tall", price: 8.50, temperature: "Hot")
ItemDetail.create(item_id: 19, size: "Tall", price: 8.50, temperature: "Cold")
ItemDetail.create(item_id: 19, size: "Grande", price: 9.55, temperature: "Hot")
ItemDetail.create(item_id: 19, size: "Grande", price: 9.55, temperature: "Cold")
ItemDetail.create(item_id: 19, size: "Venti", price: 10.60, temperature: "Hot")
ItemDetail.create(item_id: 19, size: "Venti", price: 10.60, temperature: "Cold")

# Item.create(store_id: 1, item_name: "Signature Hot Chocolate")
ItemDetail.create(item_id: 20, size: "Tall", price: 11.15, temperature: "Hot")
ItemDetail.create(item_id: 20, size: "Grande", price: 12.20, temperature: "Hot")
ItemDetail.create(item_id: 20, size: "Venti", price: 13.25, temperature: "Hot")

# Item.create(store_id: 1, item_name: "Hazelnut Hot Chocolate")
ItemDetail.create(item_id: 21, size: "Tall", price: 12.20, temperature: "Hot")
ItemDetail.create(item_id: 21, size: "Grande", price: 13.25, temperature: "Hot")
ItemDetail.create(item_id: 21, size: "Venti", price: 14.30, temperature: "Hot")

# Item.create(store_id: 1, item_name: "Caramel Hot Chocolate")
ItemDetail.create(item_id: 22, size: "Tall", price: 12.20, temperature: "Hot")
ItemDetail.create(item_id: 22, size: "Grande", price: 13.25, temperature: "Hot")
ItemDetail.create(item_id: 22, size: "Venti", price: 14.30, temperature: "Hot")

# Run.create(expired_at: Time.now + 24.hours, store_id: 1, limit: 10, remaining: 8, user_id: 1, url: "1xr45")
# Run.create(expired_at: Time.now + 24.hours, store_id: 1, limit: 10, remaining: 7, user_id: 2, url: "1xr77")
# Run.create(expired_at: Time.now + 24.hours, store_id: 2, limit: 7, remaining: 4, user_id: 2, url: "asy64")

# Order.create(run_id: 1, user_id: 2, total_store: 23.35, total_run: 24.00)
# Order.create(run_id: 1, user_id: 3, total_store: 43.45, total_run: 44.00)

# OrderItemDetail.create(item_detail_id: 1, order_id: 1, quantity: 1)
# OrderItemDetail.create(item_detail_id: 9, order_id: 1, quantity: 1)
# OrderItemDetail.create(item_detail_id: 15, order_id: 2, quantity: 1)
# OrderItemDetail.create(item_detail_id: 9, order_id: 2, quantity: 1)
# OrderItemDetail.create(item_detail_id: 18, order_id: 2, quantity: 1)

# OrderItemDetail.create(item_detail_id: 15, order_id: 4, quantity: 1)
# OrderItemDetail.create(item_detail_id: 9, order_id: 4, quantity: 1)
# OrderItemDetail.create(item_detail_id: 18, order_id: 4, quantity: 1)